import base64
import json
import os
import pickle
import re
import time

from statistic.models import TankMapping


class RawReplayParser(object):
    """Parses temp.wotreplay."""

    def __init__(self, player_account):
        self.player_account = player_account

    def parse(self, long_data):
        result = []
        splitters = ['\x80\x02\x7D\x71', '\x80\x02\x5D\x71', '\x80\x02\x4A', '\x80\x02\x28']
        for i in splitters:
            tokens = long_data.split(i)
            for block in tokens:
                block = i + block
                try:
                    pick = pickle.loads(block)
                except Exception:
                    pick = ''

                if (pick != '' and (type(pick) == tuple or type(pick) == list or type(pick) == dict)):
                    result.append(pick)

        players_data = []
        for i in result:
            try:
                if (isinstance(i, list) and isinstance(i[0], tuple) and (len(i[0]) > 10)):
                    players_data = i
            except Exception:
                pass
        for i in result:
            try:
                if (isinstance(i, tuple) and (len(i) > 10)):
                    for player in players_data:
                        if (player[2] == i[2]):
                            players_data.remove(player)
                    players_data.append(i)
            except Exception:
                pass

        teams = {}
        for i in players_data:
            tank_code = base64.b64encode(i[1][:2])
            try:
                tank = TankMapping.objects.get(code=tank_code)
            except TankMapping.DoesNotExist:
                tank = TankMapping.objects.create(
                    code=tank_code,
                    name='Unknown: {}'.format(tank_code))

            player_data = {
                'name': i[2],
                'tank': tank.name,
            }
            teams.setdefault(i[3], []).append(player_data)

        if self.player_account in [p['name'] for p in teams[2]]:
            teams[1], teams[2] = teams[2], teams[1]

        return teams
