
var analyze = function() {

    $('#status').text('Processing...');
    $('#win_chance').text('');
    $('#win_chance').removeClass();

    buildTable('#team_1');
    buildTable('#team_2');

    var files = $('#replay_file')[0].files;

    if (!files.length || files[0].size == 0) {
        $('#status').text('Battle hasn\'t started or wrong temp.wotreplay file was specified.');
        return;
    }

    var reader = new FileReader();
    reader.onload = function (e) {
        $.ajax({
            url: 'parse/',
            type: 'POST',
            timeout: 40000,
            data: {'replay_slice': e.target.result,
                   'wot_account': $('#wor_account').val()},
            success: uploadSuccess,
            error: uploadError
        });
    }

    //12000 - max needed byte
    var blob = files[0].slice(0, 12000);
    reader.readAsDataURL(blob);
}

var uploadSuccess = function(data) {
    if (data['error']) {
        $('#status').text(data['error']);
    } else if (!data[1] || !data[2]) {
        $('#status').text('No data is parsed or wrong file was specified.');
    } else {
        $('#status').text('Done.');

        buildTable('#team_1', data[1]);
        buildTable('#team_2', data[2]);

        if (data['win_chance']) {
            $('#win_chance').text('Win chance: ' + data['win_chance'] + '%');
            $('#win_chance').addClass(getWinsClass(data['win_chance']));
        } else {
            $('#win_chance').text('Could not calculate.');
        }
    }
}

var uploadError = function() {
    $('#status').text('Unexpected error...');
}

var buildTable = function(table, data) {

    var prepared_data = [];

    if (!data) {
        for (var i=0; i<15;i++){
            prepared_data.push(['', '', '', '', '', '']);
        }
    } else {
        for (var i=0; i<data.length;i++) {
            prepared_data.push([
                data[i]['name'],
                data[i]['tank'],
                data[i]['level'],
                data[i]['battles'],
                data[i]['wins'],
                data[i]['eff'],
            ]);
        }
    }
    $(table).html( '<table></table>' );
    $(table + ' table').dataTable({
        "bInfo": false,
        "bFilter": false,
        "bPaginate": false,
        "aaData": prepared_data,
        "aaSorting": [],
        "aoColumns": [
            {
                "sTitle": "Name",
                "sClass": "name_column",
                "bSortable": false,
                "bUseRendered": false,
                "fnRender": function(obj) {
                    var sReturn = obj.aData[ obj.iDataColumn ];
                    if (sReturn == $('#wor_account').val()) {
                        sReturn = "<b>" + sReturn + "</b>";
                    }
                    return sReturn;
                 },
            },
            {"sTitle": "Tank", "sClass": "tank_column", "bSortable": false},
            {"sTitle": "Level", "sClass": "level_column"},
            {
                "sTitle": "Battles",
                "sClass": "battles_column",
                "bUseRendered": false,
                "asSorting": ["desc", "asc", "asc"],
                "fnRender": function(obj) {
                    var sReturn = obj.aData[ obj.iDataColumn ];
                    return '<span class="' + getBattlesClass(sReturn) + '">' + sReturn + "</span>";
                 },
            },
            {
                "sTitle": "Wins, %",
                "sClass": "wins_column",
                "bUseRendered": false,
                "asSorting": ["desc", "asc", "asc"],
                "fnRender": function(obj) {
                    var sReturn = obj.aData[ obj.iDataColumn ];
                    return '<span class="' + getWinsClass(sReturn) + '">' + sReturn + "</span>";
                 },
            },
            {
                "sTitle": "Efficiency",
                "sClass": "efficiency_column",
                "bUseRendered": false,
                "asSorting": ["desc", "asc", "asc"],
                "fnRender": function(obj) {
                    var sReturn = obj.aData[ obj.iDataColumn ];
                    return '<span class="' + getEfficiencyClass(sReturn) + '">' + sReturn + "</span>";
                 },
            },
        ]
    });
}

var connectTables = function(table1, table2) {
    var _connectColumns = function(table1, table2, column_class) {
        $(table1 + ' th.' + column_class).live('click', function(event, triggered) {
            if (!triggered) {
                $(table2 + ' .' + column_class).trigger('click', true);
            }
        });
        $(table2 + ' th.' + column_class).live('click', function(event, triggered) {
            if (!triggered) {
                $(table1 + ' .' + column_class).trigger('click', true);
            }
        });
    }
    _connectColumns(table1, table2, 'level_column');
    _connectColumns(table1, table2, 'battles_column');
    _connectColumns(table1, table2, 'wins_column');
    _connectColumns(table1, table2, 'efficiency_column');
}

var getBattlesClass = function(battles) {
    var class_ = 'very-bad';
    if (battles >= 15000) {
        class_ = 'great';
    } else if (battles >= 10000) {
        class_ = 'good';
    } else if (battles >= 5000) {
        class_ = 'normal';
    } else if (battles >= 2000) {
        class_ = 'bad';
    }
    return class_
}

var getWinsClass = function(wins) {
    var class_ = 'very-bad';
    if (wins >= 60) {
        class_ = 'great';
    } else if (wins >= 55) {
        class_ = 'good';
    } else if (wins >= 50) {
        class_ = 'normal';
    } else if (wins >= 48) {
        class_ = 'bad';
    }
    return class_
}

var getEfficiencyClass = function(eff) {
    var class_ = 'very-bad';
    if (eff >= 1500) {
        class_ = 'great';
    } else if (eff >= 1200) {
        class_ = 'good';
    } else if (eff >= 900) {
        class_ = 'normal';
    } else if (eff >= 600) {
        class_ = 'bad';
    }
    return class_
}
