
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site


from statistic.bootstrap_data import TANKS, FLAT_PAGES
from statistic.models import Tank, CachedStatistic


class Command(BaseCommand):

    def handle(self, *args, **options):

        CachedStatistic.objects.all().delete()

        site = Site.objects.get(id=1)
        site.name = 'Wot Squad Analyzer'
        site.domain = settings.DOMAIN
        site.save()

        Tank.objects.all().delete()
        for code, tank in TANKS.items():
            Tank.objects.create(code=code, name=tank['name'],
                classification=tank['type'], level=tank['level'])

        FlatPage.objects.all().delete()
        for name, page in FLAT_PAGES.items():
            fp = FlatPage.objects.create(title=name, url=page['url'], registration_required=page['requires_login'],
                content='\n'.join(['<{element}>{content}</{element}>'.format(**c) for c in page['content']]))
            fp.sites.add(site)
            fp.save()
