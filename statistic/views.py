import base64
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

from statistic.replay_parser import RawReplayParser
from statistic.statistic_facade import StatisticFacade


@login_required
def get_statistic(request):
    return render_to_response('statistic.html',
        context_instance=RequestContext(request))


@csrf_exempt
def parse_replay(request):
    response_data = {}
    wot_account = request.POST.get('wot_account', '')

    if not request.POST.get('replay_slice'):
        return HttpResponse(
            json.dumps({'error': 'No data is given'}),
            mimetype="application/json")
    try:
        replay = base64.b64decode(request.POST['replay_slice'].split(',')[1])
        response_data = RawReplayParser(wot_account).parse(replay)
    except Exception as why:
        return HttpResponse(
            json.dumps({'error': 'Error while exctracting replay',
                        'error_data': str(why)}),
            mimetype="application/json")

    statistic = StatisticFacade()
    statistic.extract_statistic(response_data)
    statistic.calculate_win_change(response_data)

    return HttpResponse(json.dumps(response_data), mimetype="application/json")
