
from django.db import models


class TankMapping(models.Model):

    code = models.CharField(max_length=10, unique=True, blank=False)
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return unicode(self.name)
