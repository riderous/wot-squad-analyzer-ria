import json
import time
import requests

from Queue import Queue
from threading import Thread

from django.conf import settings


PLAYER_ID_QUERY_URL = 'http://worldofclans.ru/search/PlayerSearch.html?term={}'
PLAYER_STAT_QUERY_URL = 'http://worldoftanks.ru/uc/accounts/{}/api/1.8/?source_token=Intellect_Soft-WoT_Mobile-unofficial_stats'


class StatisticFacade(object):

    def extract_statistic(self, teams_data):
        pool = DownloaderPool(settings.DOWNLOADERS_COUNT)

        for team in teams_data.values():
            for p in team:
                pool.add_task(p)

        pool.wait_completion()

    def calculate_win_change(self, teams_data):
        if not teams_data or not teams_data.get(1) or not teams_data.get(2):
            return

        ef1 = [p['eff'] for p in teams_data[1]]
        ef2 = [p['eff'] for p in teams_data[2]]

        if (ef1 + ef2).count(0) > 3:
            return

        av_ef1 = float(sum(ef1) / len(teams_data[1]))
        av_ef2 = float(sum(ef2) / len(teams_data[2]))

        win_chance = 50 + (av_ef1 - av_ef2) / av_ef1 * 100
        if win_chance > 100:
            win_chance = 100
        teams_data['win_chance'] = round(win_chance, 2)


class DownloaderPool(object):

    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads): Downloader(self.tasks)

    def add_task(self, player):
        self.tasks.put(player)

    def wait_completion(self):
        self.tasks.join()


class Downloader(Thread):

    def __init__(self, tasks):
        Thread.__init__(self)
        self.daemon = True
        self.tasks = tasks
        self.start()

    def run(self):
        while True:
            player = self.tasks.get()

            #Retry logic
            for i in xrange(settings.DOWNLOADERS_RETRIES):
                statistic = self._get_player_stat(player['name'])
                if statistic:
                    break
                else:
                    time.sleep(settings.DOWNLOADERS_SLEEP)
            player.update(statistic)
            self.tasks.task_done()

    def _get_player_stat(self, player_name):
        id_ = self._get_player_id(player_name)
        if id_ is None:
            return

        r = requests.get(PLAYER_STAT_QUERY_URL.format(id_)).json
        if not r or r.get('status', '').lower() != 'ok':
            return

        try:
            battles = r['data']['summary']['battles_count']
            wins = r['data']['summary']['wins'] / float(battles)

            return {
                'battles': battles,
                'wins': round(wins * 100, 2),
                'eff': 0
            }
        except KeyError:
            return

    def _get_player_id(self, player_name):
        try:
            r = requests.get(PLAYER_ID_QUERY_URL.format(player_name))
            return json.loads(r.text[3:])[0]['id']
        except Exception:
            return None
