
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic.simple import redirect_to

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', redirect_to, {'url': '/statistic/'}),

    url(r'^statistic/$', 'statistic.views.get_statistic'),
    url(r'^statistic/parse/$', 'statistic.views.parse_replay'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login'),
    url(r'^pages/', include('django.contrib.flatpages.urls')),

    url(settings.STATIC_URL[1:] + r'(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
