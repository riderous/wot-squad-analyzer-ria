import dj_database_url


ADMINS = (
    ('Alex Hmelevsky', 'alex.hmelevsky@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {}

DEBUG = False
TEMPLATE_DEBUG = DEBUG

DOMAIN = 'wot-squad-analyzer.herokuapp.com'

DATABASES['default'] =  dj_database_url.config()

LANGUAGE_CODE = 'en-us'

SITE_ID = 1
USE_I18N = True

MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = 'media'
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = ')b_gl%m%8429q%s)!0iav1tbp02v(6qr_32b1on-8wnk1dw2vt'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'wotsquadanalyzer.urls'

WSGI_APPLICATION = 'wotsquadanalyzer.wsgi.application'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.flatpages',
    'statistic'
)

# Number of donwloader instances.
DOWNLOADERS_COUNT = 10

# Timeout of getting statistic.
DOWNLOADERS_TIMEOUT = 5

# Numbers of retry to get statistic.
DOWNLOADERS_RETRIES = 5

# Time to sleep between unsuccessful tries (in seconds)
DOWNLOADERS_SLEEP = 0.1

# Maximum number of cached statistic (None to infinite)
CACHE_SIZE = 8000

try:
    from local_settings import *
except ImportError:
    pass
